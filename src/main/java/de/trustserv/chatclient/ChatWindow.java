package de.trustserv.chatclient;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Runtime.getRuntime;
import static java.lang.Thread.sleep;
import static java.util.logging.Logger.getLogger;
import static javax.swing.SwingUtilities.invokeLater;

/**
 * @author oli
 */
public class ChatWindow extends javax.swing.JFrame {

    // End of variables declaration//GEN-END:variables
    private static final Logger LOG = Logger.getLogger(ChatWindow.class.getName());
    private final CloseableHttpClient client;
    private final Account account;
    private StringBuilder b = new StringBuilder();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    ChatWindow(final CloseableHttpClient httpClient, final Account account) {
        initComponents();
        this.client = httpClient;
        this.account = account;

        getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    String url = "https://" + account.getServer() + ".webkicks.de/cgi-bin/chat.cgi";
                    HttpPost logoutPost = new HttpPost(url);
                    StringEntity ent = null;
                    try {
                        ent = new StringEntity("user=" + account.getUser().getNick() + "&pass=" + account.getPass() + "&cid=" + account.getCid() + "&message=%2Fexit");
                    } catch (UnsupportedEncodingException ex) {
                        getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    logoutPost.setEntity(ent);
                    HttpGet get = new HttpGet("https://" + account.getServer() + ".webkicks.de/" + account.getCid() + "/kill/" + account.getUser().getNick() + "/" + account.getPass());
                    httpClient.execute(get);
                } catch (IOException ex) {
                    getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        setVisible(true);

    }

    /**
     *
     */
    public void readerLoop() {
        String streamUrl = account.getStreamUrl();
        HttpGet get = new HttpGet(streamUrl);

        jEditorPane1.setAutoscrolls(true);
        jScrollPane1.setAutoscrolls(true);
        jScrollPane2.setAutoscrolls(true);

        HTMLEditorKit kit = new HTMLEditorKit();
        HTMLDocument doc = new HTMLDocument();
        jEditorPane1.setEditorKit(kit);
        jEditorPane1.setDocument(doc);

        try {
            HttpEntity entity = client.execute(get).getEntity();
            final InputStreamEntity stream = new InputStreamEntity(entity.getContent(), entity.getContentLength());

            final ByteArrayOutputStream ost = new ByteArrayOutputStream(32_768);
            new Thread() {
                @Override
                public void run() {
                    try {
                        stream.writeTo(ost);
                    } catch (IOException ex) {
                        getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }.start();

            while (true) {
                if (ost.size() != 0) {
                    final String msg = ost.toString();


                    if (msg.matches("(?s)<!-- update //-->.*")) {
                        ost.reset();
                        continue;
                    }


                    if (msg.contains("<font title=\"")) {
                        final String msg2 = msg.replaceAll("<img src=\"/" + account.getCid() + "/replacer/", "<img src=\"https://" + account.getServer() + ".webkicks.de/" + account.getCid() + "/replacer/").
                                replaceAll("<td.*?>", "").replaceAll("</td>", "").replaceAll("<table.*?>", "").replaceAll("</table>", "").replaceAll("<tr>", "").replaceAll("</tr>", "");
                        addMsgToEditorPane(msg2, (HTMLEditorKit) jEditorPane1.getEditorKit(), (HTMLDocument) jEditorPane1.getDocument());
                    } else if (msg.contains("<script language=\"JavaScript\">parent.rightFrame.addp")) {
                        addMsgToEditorPane(msg.replaceAll("\\<.*?\\>", ""), (HTMLEditorKit) jEditorPane1.getEditorKit(), (HTMLDocument) jEditorPane1.getDocument());
                    } else if (msg.contains("Ohne weitere Eingabe erfolgt in 2 Minuten dein Timeout.")) {
                        String postUrl = "https://" + account.getServer() + ".webkicks.de/cgi-bin/chat.cgi";
                        HttpPost post = new HttpPost(postUrl);
                        StringEntity ent = new StringEntity("AutoScroll=on&user=" + account.getUser().getNick() + "&pass=" + account.getPass() + "&cid=" + account.getCid() + "&message=" + "-");
                        post.setEntity(ent);
                        client.execute(post);
                        jTextField1.setText("");
                    } else {
                    }
                    ost.reset();
                }
                sleep(250);
            }

        } catch (IOException | IllegalStateException | InterruptedException ex) {
            getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addMsgToEditorPane(final String msg, final HTMLEditorKit kit, final HTMLDocument doc) {
        new Thread() {
            @Override
            public void run() {
                invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        //jEditorPane1.setText(b.toString());
                        //HTMLDocument document = (HTMLDocument) jEditorPane1.getDocument();
                        try {
                            //document.setParser(new ParserDelegator());
                            //document.insertBeforeEnd(document.getDefaultRootElement(), msg2);
                            synchronized (ChatWindow.class) {
                                kit.insertHTML(doc, doc.getLength(), msg, 0, 0, null);

                                sleep(20);
                                jEditorPane1.setCaretPosition(jEditorPane1.getDocument().getLength());
                            }
                        } catch (BadLocationException | IOException | InterruptedException ex) {
                            getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
            }
        }.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jButton1.setText("send");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane2.setAutoscrolls(true);
        jScrollPane2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane2.setDoubleBuffered(true);

        jEditorPane1.setEditable(false);
        jEditorPane1.setContentType("text/html"); // NOI18N
        jScrollPane2.setViewportView(jEditorPane1);

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};

            public int getSize() {
                return strings.length;
            }

            public Object getElementAt(int i) {
                return strings[i];
            }
        });
        jScrollPane1.setViewportView(jList1);

        jButton2.setText("clear");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                                                        .addComponent(jScrollPane2))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(0, 0, Short.MAX_VALUE)
                                                                .addComponent(jButton1))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, Short.MAX_VALUE))))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButton2)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(jButton2)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane2)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton1))
                                .addGap(13, 13, 13))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            // TODO add your handling code here:
            String postUrl = "https://" + account.getServer() + ".webkicks.de/cgi-bin/chat.cgi";
            HttpPost post = new HttpPost(postUrl);
            StringEntity ent = new StringEntity("AutoScroll=on&user=" + account.getUser().getNick() + "&pass=" + account.getPass() + "&cid=" + account.getCid() + "&message=" + jTextField1.getText());
            post.setEntity(ent);
            client.execute(post);
            jTextField1.setText("");
        } catch (IOException ex) {
            getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                // TODO add your handling code here:
                String postUrl = "https://" + account.getServer() + ".webkicks.de/cgi-bin/chat.cgi";
                HttpPost post = new HttpPost(postUrl);
                StringEntity ent = new StringEntity("AutoScroll=on&user=" + account.getUser().getNick() + "&pass=" + account.getPass() + "&cid="
                        + account.getCid() + "&message=" + jTextField1.getText());
                post.setEntity(ent);
                client.execute(post);
                jTextField1.setText("");
            } catch (IOException ex) {
                getLogger(ChatWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        b = new StringBuilder();
        HTMLEditorKit kit = new HTMLEditorKit();
        HTMLDocument doc = new HTMLDocument();
        jEditorPane1.setEditorKit(kit);
        jEditorPane1.setDocument(doc);

    }//GEN-LAST:event_jButton2ActionPerformed
}
