package de.trustserv.chatclient;

import java.util.logging.Logger;

/**
 * @author oli
 */
public class Account {

    private static final Logger LOG = Logger.getLogger(Account.class.getName());
    User user;
    private String cid;
    private String job = "ok";
    private String login = "Login";
    private String streamUrl;
    private String server;

    Account(User user, String cid, String server) {
        this.user = user;
        this.cid = cid;
        this.server = server;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return user.getPassword();
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.user.setPassword(pass);
    }

    /**
     * @return the cid
     */
    public String getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(String cid) {
        this.cid = cid;
    }

    /**
     * @return the job
     */
    public String getJob() {
        return job;
    }

    /**
     * @param job the job to set
     */
    public void setJob(String job) {
        this.job = job;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the streamUrl
     */
    public String getStreamUrl() {
        return streamUrl;
    }

    void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    /**
     * @return the server
     */
    public String getServer() {
        return server;
    }

    /**
     * @param server the server to set
     */
    public void setServer(String server) {
        this.server = server;
    }
}
