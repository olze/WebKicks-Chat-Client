/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.trustserv.chatclient;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

/**
 * @author oli
 */
public class User {
    private static final Logger LOG = Logger.getLogger(User.class.getName());
    private String nick;
    private String password;
    private Color color;
    private Icon avatar;


    /**
     * @param nick
     * @param password
     */
    public User(String nick, String password) {
        this.nick = nick;
        this.password = password;
    }

    /**
     * @return the nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * @param nick the nick to set
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
