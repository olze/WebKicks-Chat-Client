package de.trustserv.chatclient;

import org.apache.http.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.Args;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;
import static java.util.logging.Logger.getLogger;

/**
 * @author oli
 */
public class ChatClient {

    private static final Logger LOG = Logger.getLogger(ChatClient.class.getName());
    private final CloseableHttpClient httpClient;
    private Account account;

    /**
     *
     */
    public ChatClient() {

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        // Increase max total connection to 200
        cm.setMaxTotal(200);
        // Increase default max connection per route to 20
        cm.setDefaultMaxPerRoute(20);
        // Increase max connections for localhost:80 to 50
        HttpHost localhost = new HttpHost("locahost", 80);
        cm.setMaxPerRoute(new HttpRoute(localhost), 50);

        httpClient = HttpClients.custom()
                .setConnectionManager(cm)
                .setRedirectStrategy(new MyDefaultRedirectStrategy())
                .build();
    }

    private static class MyDefaultRedirectStrategy extends DefaultRedirectStrategy {
        public boolean isRedirected(
                final HttpRequest request,
                final HttpResponse response,
                final HttpContext context) throws ProtocolException {
            Args.notNull(request, "HTTP request");
            Args.notNull(response, "HTTP response");

            final int statusCode = response.getStatusLine().getStatusCode();
            final String method = request.getRequestLine().getMethod();
            final Header locationHeader = response.getFirstHeader("location");
            switch (statusCode) {
                case HttpStatus.SC_MOVED_TEMPORARILY:
                    return isRedirectable(method) && locationHeader != null;
                case HttpStatus.SC_MOVED_PERMANENTLY:
                case HttpStatus.SC_TEMPORARY_REDIRECT:
                    return true;
                case HttpStatus.SC_SEE_OTHER:
                    return true;
                default:
                    return false;
            }
        }
    }

    void showLoginForm() {
        LoginForm form = new LoginForm();
        form.setModalExclusionType(null);
        form.setVisible(true);

        while (account == null) {
            account = form.getAccount();
            try {
                sleep(100);
            } catch (InterruptedException ex) {
                getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        form.dispose();

    }

    void login() {

        String url = "https://" + account.getServer() + ".webkicks.de/" + account.getCid() + "/";
        HttpPost loginPost = new HttpPost(url);
        try {
            loginPost.setEntity(new StringEntity("user=" + account.getUser().getNick() + "&pass=" + account.getPass() + "&job=ok&cid=" + account.getCid() + "&login=Login"));
        } catch (UnsupportedEncodingException ex) {
            getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        HttpResponse execute = null;
        try {
            execute = httpClient.execute(loginPost);
        } catch (IOException ex) {
            getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String site = EntityUtils.toString(execute.getEntity());
            int index = site.indexOf("/" + account.getUser().getNick() + "/") + account.getUser().getNick().length() + 2;
            int index2 = site.indexOf('/', index + 1);
            String pw = site.substring(index, index2);
            account.setPass(pw); //thats what we need the whole time
            String streamSite = "https://" + account.getServer() + ".webkicks.de/" + account.getCid() + "/chatstream/" + account.getUser().getNick() + "/" + account.getPass() + "/start";
            HttpGet get = new HttpGet(streamSite);
            HttpResponse execute1 = httpClient.execute(get);
            HttpEntity entity = execute1.getEntity();
            String streamUrl = EntityUtils.toString(entity);
            streamUrl = streamUrl.split("URL=")[1].split("\">")[0];
            account.setStreamUrl(streamUrl);
        } catch (IOException | ParseException ex) {
            getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        ChatWindow window = new ChatWindow(httpClient, account);
        window.readerLoop();

    }

    void work() {
    }
}
