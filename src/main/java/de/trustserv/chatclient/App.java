package de.trustserv.chatclient;

import java.util.logging.Logger;

/**
 * Hello world!
 */
public class App {

    private static final Logger LOG = Logger.getLogger(App.class.getName());

    /**
     * @param args
     */
    public static void main(String[] args) {
        final ChatClient client = new ChatClient();

        client.showLoginForm();
        client.login();
        client.work();
    }
}
